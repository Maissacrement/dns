#!make

DOCKER:= @docker
IMAGE=dns
REGISTRY=registry.gitlab.com/maissacrement
VERSION=1.0.0

prod:
        ${DOCKER} run -it --rm ${REGISTRY}/${IMAGE}:latest /bin/bash

exec_running_container:
        ${DOCKER} exec -it $(docker ps -f 'name=${IMAGE}' -q) /bin/bash
