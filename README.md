# dns

dns server automate

## Getting started

`IP`: server dns ip

`CIDR`: ip address range

`DNS_BASE`: dns server name base

`DNS[:N]`: subdomain dns server bound definition, where N is an incremental number of subdomain

`ETC`: default '/etc' etc path

`BIND_DIR`: default '/etc/bind' bind path
